---
layout: post
title: "Old Hobby: Woodburning"
tags: [just for fun,woodburning]
---

# Old Hobby: Woodburning

Almost exactly three years ago, I started my first woodburning project, a gift for some good friends of mine who were soon going to be visiting for a weekend. Behold, in all its glory, the initial pencil outline.

{% include image.html name="lj5-outline.jpg" caption="Image Caption!" %}